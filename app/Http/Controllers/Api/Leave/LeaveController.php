<?php

namespace App\Http\Controllers\Api\Leave;

use Mail;
use App\Model\User;
use Illuminate\Http\Request;
use App\Model\Staff;
use App\Model\LeaveApplied;
use App\Model\LeaveType;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Controllers\leave\LeaveController as LeaveService;
use Carbon\Carbon;
use DateTime;
use App\Model\LeaveBalance;

class LeaveController extends Controller
{
    const DEFAULT_SUBJECT_APPLIED = "Someone Has Applied for a Leave";

    protected function getLeaveInformation()
    {
        $leave = LeaveApplied::countLeaveApplied();

        return response()->json($leave);
    }

    protected function setAppliedLeave(Request $request)
    {
        $subject = self::DEFAULT_SUBJECT_APPLIED;
        $input = $request->input();
        $userId = $input['userid'];
        $leaveType = $input['leave_type'];
        $reason = $input['reason'];
        $user = User::select('staff_id')->find($userId);
        $staff = Staff::find($user['staff_id']);
        $organizationId = $staff['organization_id'];

        $startdate = LeaveService::dateFormatYmd($input['start_date']);
        $enddate = LeaveService::dateFormatYmd($input['end_date']);
        $datediff = LeaveService::dateDifference($startdate, $enddate);

        $leaveApplied = LeaveApplied::create([
            'staff_id' => $user['staff_id'],
            'leave_type' => $leaveType,
            'reason' => $reason,
            'start_date' => $startdate,
            'end_date' => $enddate,
            'leave_day' => $datediff,
            'applied_by' => $userId,
            'status' => 3,
            'date_applied' => date('Y-m-d'),
        ]);
        if ($leaveApplied) {
            $userLevel = [1, 4, 5];
            $users = User::getHigherUser($organizationId, $userLevel);
            $data = [];
            $leaveType = LeaveType::find($leaveType);
            $leaveName = $leaveType['name'];
            foreach ($users as $user) {
                $data = array(
                    array(
                        "email" => $user['email'],
                        "name" => $user['name'],
                        "leave_type" => $leaveName,
                        "leave_day" => $datediff,
                        "start_date" => $startdate,
                        "end_date" => $enddate,
                    ),
                );
            }

            foreach ($data as $dt) {
                $mail = $dt['email'];
                Mail::send('mail.leave.apply', $dt, function ($message) use ($subject, $mail) {
                    $message->to($mail)->subject($subject);
                });
            }

            $resp = array(
                'status' => 'ok',
                'msg' => 'Your application successfully sent',
            );

            return response()->json($resp);
        } else {
            $resp = array(
                'status' => 'error',
                'msg' => 'Something went wrong, Please try again later',
            );

            return response()->json($resp);
        }
    }

    protected function myAllLeave(Request $request)
    {
        $input = $request->input();
        $userId = $input['userid'];
        $myAppliedLeave = LeaveApplied::getAllMyLeave($userId);
        $leaveListPending = null;
        $leaveArray = array(
            'allMyLeave' => $myAppliedLeave,
        );

        return response()->json($leaveArray);
    }
}
