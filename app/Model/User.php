<?php

namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    protected $table = 'users';

    protected $fillable = ['username', 'email', 'password', 'staff_id', 'level', 'status'];
    protected $hidden = ['password', 'remember_token'];

    public static function getHigherUser($oraganizationId, $userLevel)
    {
        return User::select('users.email', 'staff.name')
            ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
            ->where('staff.organization_id', $oraganizationId)
            ->whereIn('users.level', $userLevel)
            ->get();
    }

    public function staff()
    {
        return $this->belongsTo('App\Model\Staff');
    }

    public static function getUserCompany($id)
    {
        return User::leftJoin('staff as s', 's.id', '=', 'users.staff_id')->leftJoin('organization as c', 'c.id', '=',
            's.organization_id')->where('users.id', $id)->first();
    }

    public static function getUserprofile($id)
    {
        return User::leftJoin('staff as s', 's.id', '=', 'users.staff_id')->leftJoin('staff_profile as sp',
            'sp.staff_id', '=', 's.id')->where('users.id', $id)->first();
    }

    public static function getUserByEmailLogin($email)
    {
        return User::where('username', '=', Input::get('login_email'))->get();
    }
}
