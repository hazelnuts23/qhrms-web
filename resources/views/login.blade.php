<!DOCTYPE html ng-app="qhrms">
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Quick Human Resource Management System</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Ai Human Resouce Management System">
    <meta name="author" content="SQAI Technologies">
    <link rel="stylesheet" href="{{ URL::asset('css/styles.css')}}">
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/angular.min.js" type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
</head>
<body class="focusedform" ng-controller="login">

<div class="verticalcenter">
    <a href="/"><img src="{{ URL::asset('img/logo-big.png') }}" alt="Logo" class="brand"/></a>

    <div class="panel panel-primary">
        <div class="panel-body">
            <h4 class="text-center" style="margin-bottom: 25px;"></h4>

            <form id="login-action" action="{{ URL::to('login') }}" class="form-horizontal" method="post">
                <input type="hidden" name="_token" ng-model="csrf_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label for="email" class="control-label col-sm-4" style="text-align: left;">Email</label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="login_email" id="email" placeholder="Email"
                               ng-model="login_email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label col-sm-4" style="text-align: left;">Password</label>

                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="login_password" id="password"
                               placeholder="Password" ng-model="login_password">
                    </div>
                </div>

                <button type="button" class="btn btn-primary btn-block" ng-click="loginBtn()">Log In</button>
            </form>
        </div>
        <div class="panel-footer">

            <div class="pull-right">
                <a href="#" class="btn btn-default">Reset</a>
            </div>
        </div>
    </div>
</div>
<script src="/plugins/blockui/jquery.blockUI.js" type="text/javascript"></script>
<script src="/js/controller/login.js" type="text/javascript"></script>
</body>
</html>