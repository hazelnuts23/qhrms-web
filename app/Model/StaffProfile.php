<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StaffProfile extends Model
{
    protected $table = 'staff_profile';
    protected $primaryKey = 'id';

    public function Staff()
    {
        return $this->belongsTo('App\Model\Staff', 'staff_id');
    }
}
