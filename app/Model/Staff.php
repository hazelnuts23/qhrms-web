<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'name'];

    public function staffProfile()
    {
        return $this->hasOne('App\Model\StaffProfile');
    }

    public function user()
    {
        return $this->hasOne('App\Model\User');
    }
}
