<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\LeaveType;
use App\LeaveBalance;
use DB;

class ClaimApplied extends Model
{
    protected $table = 'claim_applied';
    protected $primaryKey = 'id';

//    public static function getAppliedList()
//    {
//        return LeaveApplied::Select('*', 'leave_applied.status As leave_status', 'leave_applied.id As leave_id')->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
//            ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
//            ->leftJoin('staff_profile as sp', 'sp.staff_id', '=', 'staff.id')
//            ->get();
//    }
//
//    public static function getAppliedByUserID($val)
//    {
//        return LeaveApplied::Select('*', 'leave_applied.status As leave_status', 'leave_applied.id As leave_id')->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
//            ->leftJoin('user_profile', 'user_profile.id', '=', 'users.profile_id')
//            ->where('leave_applied.applied_by', '=', $val)
//            ->get();
//    }
//    public static function getLeaveBalance($applied_by, $type){
//        $type = LeaveBalance::where('leave_type', '=', $type)->where('staff_id', '=', $applied_by)->first();
//
//        $balance = LeaveApplied::select(DB::raw('sum(leave_day) as leave_sum'))->where('leave_type', $type->id)->where('applied_by', $applied_by)->where('status', 1)->first();
//        $bl = $type->balance;
//        $bd = $balance->leave_sum;
//        $final = $bl-$bd;
//        return $final;
//    }
}
