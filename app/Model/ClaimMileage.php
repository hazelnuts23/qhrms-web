<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\LeaveType;
use App\LeaveBalance;
use DB;

class ClaimMileage extends Model
{
    protected $table = 'claim_mileage';
    protected $primaryKey = 'id';
}
