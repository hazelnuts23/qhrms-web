<?php

namespace App\Model;

use App\Model\LeaveType;
use Illuminate\Database\Eloquent\Model;

class LeaveBalance extends Model
{
    protected $table = 'leave_balance';
    protected $primaryKey = 'id';
    protected $fillable = ['staff_id', 'balance', 'year', 'leave_type', 'flag'];

    public static function allLeaveBalanceApi($userId)
    {
        return self::select('leave_balance.balance', 'leave_type.name')
            ->leftJoin('staff', 'staff.id', '=', 'leave_balance.staff_id')
            ->leftJoin('users', 'users.staff_id', '=', 'staff.id')
            ->leftJoin('leave_type', 'leave_type.id', '=', 'leave_balance.leave_type')
            ->where('users.id', $userId)
            ->get();


    }
}
