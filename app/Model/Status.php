<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';
    protected $primaryKey = 'id';

    public static function getDescription($val){
        return Status::where('id', $val)->first();
    }
}
