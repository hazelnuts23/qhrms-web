
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-orange">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 clearfix">
                        <h4 class="pull-left" style="margin: 0 0 20px;">Hi There, {{ Auth::user()->staff->name }}!</h4>
                    </div>
                    <div class="col-md-12">

                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <h3 style="text-align: center; margin: 0; color: #808080;"><?php echo date("d/m/Y H:i:s"); ?></h3>

                            <p style="text-align: center; margin: 0;">Last Login</p>
                            <hr class="hidden-md hiddØen-lg">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <h3 style="text-align: center; margin: 0; color: #808080;">@if(isset(Auth::user()->login_fail)) {{ date("d/m/Y H:i:s", strtotime(Auth::user()->login_fail)) }} @else {{ '-' }} @endif</h3>

                            <p style="text-align: center; margin: 0;">Fail Login</p>
                            <hr class="hidden-md hidden-lg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6">
                <a class="info-tiles tiles-toyo" href="{{ URL::to('leave/my-leave') }}">
                    <div class="tiles-heading">Leave Applied</div>
                    <div class="tiles-body-alt">
                        <div class="text-center">2</div>
                        <small>Total of Leave to be Authorize</small>
                    </div>
                    <div class="tiles-footer">check my leave</div>
                </a>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6">
                <a class="info-tiles tiles-success" href="#">
                    <div class="tiles-heading">Total Claim</div>
                    <div class="tiles-body-alt">
                        <!--i class="fa fa-money"></i-->
                        <div class="text-center"><span class="text-top">RM</span>357.00</div>
                        <small>Total of Unprocessed Claim</small>
                    </div>
                    <div class="tiles-footer">check my claim</div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-midnightblue">
            <div class="panel-heading">
                <h4>Memo</h4>
                <!-- <div class="options">
                  <a href="javascript:;"><i class="fa fa-cog"></i></a>
                  <a href="javascript:;"><i class="fa fa-wrench"></i></a>
                </div> -->
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="threads">
                        <ul class="panel-threads">
                            @foreach($content as $conval)
                                <li>
                                    <div class="content">
                                        <h4 class="title bold">{{ $conval->title }}</h4>
                                        <span class="desc">{{ $conval->text }}</span>
                                        <span class="time">{{ date("d/m/Y H:i:s", strtotime($conval->datePublished)) }}</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <a href="#" class="btn btn-default-alt btn-sm pull-right">Load More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4><i class="icon-highlight fa fa-calendar"></i> Calendar</h4>

                <div class="options">
                    <a href="javascript:;"><i class="fa fa-cog"></i></a>
                    <a href="javascript:;"><i class="fa fa-wrench"></i></a>
                    <a href="javascript:;" class="panel-collapse"><i class="fa fa-chevron-down"></i></a>
                </div>
            </div>
            <div class="panel-body" id="calendardemo">
                <div id='calendar-drag'></div>
            </div>
        </div>
    </div>

</div>
