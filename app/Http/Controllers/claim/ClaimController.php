<?php

namespace App\Http\Controllers\claim;

use Illuminate\Support\Facades\Redirect;
use Request;
use Response;
use App\Http\Controllers\Controller;
use App\Model\ClaimApplied;
use App\Model\ClaimMileage;
use App\Model\LeaveApplied;
use App\Model\LeaveType;
use Auth;
use View;
use Validator;
use Input;
use URL;
use Carbon\Carbon;
use App\User;


class ClaimController extends Controller
{
    public function myclaim()
    {
        $breadcrumb = array(
            0 => array('name' => 'Claim', 'url' => null),
            1 => array('name' => 'My Claim', 'url' => 'leave/my-claim'));
        $heading_title = 'My Leave';
        $leave = LeaveApplied::getAppliedList();
        return View::make('private.index', array('breadcrumb' => $breadcrumb, 'headingtitle' => $heading_title))
            ->nest('content', 'private.claim.my-claim', array('myleave' => $leave))
            ->nest('header_script', 'private.base.headerscript')
            ->nest('footer_script', 'private.base.footerscript_all');
    }

    public function applyleave()
    {
        $breadcrumb = array(
            0 => array('name' => 'Leave', 'url' => null),
            1 => array('name' => 'Apply Leave', 'url' => 'leave/apply-leave'));
        $heading_title = 'Apply Leave';
        $leave = LeaveApplied::getAppliedList();
        $leavetype = LeaveType::all();
        return View::make('private.index', array('breadcrumb' => $breadcrumb, 'headingtitle' => $heading_title))
            ->nest('content', 'private.leave.apply-leave', array('leavetype' => $leavetype))
            ->nest('header_script', 'private.base.headerscript_form')
            ->nest('footer_script', 'private.base.footerscript_form');
    }

    public function savemyleave(Request $request)
    {
        $rules = array(
            'leavetype' => 'required',
            'reason' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $response = array(
                'status' => 0,
                'redirect' => URL::to('leave/apply-leave'),
            );
            return response()->json($response)->withErrors($validator->errors());
        } else {
            $startdate = LeaveController::dateFormatYmd(Input::get('start_date'));
            $enddate = LeaveController::dateFormatYmd(Input::get('end_date'));
            $datediff = LeaveController::dateDifference($startdate, $enddate);
            $leave = new LeaveApplied();
            $leave->leave_type = Input::get('leavetype');
            $leave->applied_by = Auth::user()->id;
            $leave->date_applied = date('Y-m-d');
            $leave->status = 3;
            $leave->start_date = LeaveController::dateFormatYmdHis(Input::get('username'));;
            $leave->end_date = LeaveController::dateFormatYmdHis(Input::get('end_date'));;
            $leave->leave_day = $datediff;
            $leave->reason = Input::get('reason');
            $leave->remarks = Input::get('remarks');
            $leave->save();
            $response = array(
                'status' => 1,
                'redirect' => URL::to('leave/my-leave'),
            );
            return response()->json($response);
        }



    }

    public function editLeave($id)
    {
        $breadcrumb = array(
            0 => array('name' => 'Applications', 'url' => 'leave/my-leave'),
            1 => array('name' => 'Edit', 'url' => null));
        $heading_title = 'Update Leave';
        $leave = LeaveApplied::find($id);
        $leavetype = LeaveType::select('name', 'id')->get();
        $applicant = User::getUserProfile($leave->applied_by);
        $balance = LeaveApplied::getLeaveBalance(Auth::user()->id, $leave->leave_type);
        $reason = $leave->reason;
        $start_date = LeaveController::dateFormatdMY($leave->start_date);
        $end_date = LeaveController::dateFormatdMY ($leave->end_date);
        $remarks = $leave->remarks;
        $leave_type = $leave->leave_type;
        return View::make('private.index', array('breadcrumb' => $breadcrumb, 'headingtitle' => $heading_title))
            ->nest('content', 'private.leave.update-leave', array('id'=>$id, 'leavetype' => $leavetype, 'reason' => $reason, 'start_date' => $start_date, 'end_date' => $end_date, 'remarks' => $remarks, 'leave_type' => $leave_type))
            ->nest('header_script', 'private.base.headerscript_form')
            ->nest('footer_script', 'private.base.footerscript_form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function updateLeave(Request $request, $id)
    {
        $rules = array(
            'leavetype' => 'required',
            'reason' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $response = array(
                'status' => 0,
                'redirect' => URL::to('leave/edit-leave'),
            );
            return response()->json($response)->withErrors($validator->errors());
        } else {

            $startdate = LeaveController::dateFormatYmd(Input::get('start_date'));
            $enddate = LeaveController::dateFormatYmd(Input::get('end_date'));
            $datediff = LeaveController::dateDifference($startdate, $enddate);
            $leave = LeaveApplied::find($id);
            $leave->leave_type = Input::get('leavetype');
            $leave->applied_by = Auth::user()->id;
            $leave->date_applied = date('Y-m-d');
            $leave->status = 3;
            $leave->start_date = LeaveController::dateFormatYmdHis(Input::get('start_date'));;
            $leave->end_date = LeaveController::dateFormatYmdHis(Input::get('end_date'));;
            $leave->leave_day = $datediff;
            $leave->reason = Input::get('reason');
            $leave->remarks = Input::get('remarks');
            $leave->save();
            $response = array(
                'status' => 1,
                'redirect' => URL::to('leave/my-leave'),
            );
            return response()->json($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $leave = LeaveApplied::find($id);
        $leave->delete();

        return Redirect::to('leave/my-leave');
    }

    public function approve($id)
    {
        $leave = LeaveApplied::find($id);
        $leave->status = 1;
        $leave->save();

        return Redirect::to('leave/my-leave');
    }

    public function reject($id)
    {
        $leave = LeaveApplied::find($id);
        $leave->status = 2;
        $leave->save();

        return Redirect::to('leave/my-leave');
    }

    public function leavebalance($id, $leave_type)
    {
        $balance = LeaveApplied::getLeaveBalance($id, $leave_type);
        if (isset($balance)) {
            $response = array(
                'status' => 1,
                'value' => $balance,
            );

            return response()->json($response);
        } else {
            $response = array(
                'status' => 0,
                'value' => $balance,
            );

            return response()->json($response);
        }
    }

    //Convert date to standard dd/mm/YYYY format
    public function dateFormatdMY($val)
    {
        return date("d/m/Y", strtotime($val));
    }

    public function dateFormatdMYHis($val)
    {
        return date("d/m/Y H:i:s", strtotime($val));
    }

    // Usually this part to convert date to SQL format
    public function dateFormatYmdHis($val)
    {
        $newdate = Carbon::createFromFormat('d/m/Y', $val);
        return $newdate->format('Y-m-d H:i:s');
    }

    public function dateFormatYmd($val)
    {
        $newdate = Carbon::createFromFormat('d/m/Y', $val);
        return $newdate->format('Y-m-d');
    }

    public function dateDifference($date_1, $date_2, $differenceFormat = '%a')
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);

    }
}
