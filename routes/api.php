<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(array(['middleware' => 'jwt.auth']), function () {
    Route::post('/dashboard', 'Dashboard\DashboardController@index');
    Route::post('/login', 'System\UserController@login');
    Route::post('/leave/apply', 'Leave\LeaveController@setAppliedLeave');
    Route::post('/leave/my-all', 'Leave\LeaveController@myAllLeave');

//    Route::get('/user', 'User\UserController@getUserInfo');
//    Route::get('/dashboard', 'Dashboard\DashboardController@index');
//
//    /* Leave */
//    /*My Leave*/
//    Route::get('/leave/my-leave', 'Leave\LeaveController@myleave');
//    Route::post('/leave/my-leave/save-leave', 'Leave\LeaveController@savemyleave');
//    Route::post('/leave/my-leave/{id}/{leave_type}/check_balance', 'Leave\LeaveController@leavebalance');
//    Route::post('/leave/my-leave/{id}/delete', 'Leave\LeaveController@destroy');
//
//    /*Apply Leave*/
//    Route::get('/leave/apply-leave', 'Leave\LeaveController@applyleave');
//    Route::post('/leave/save-leave', 'Leave\LeaveController@savemyleave');
//
//    /*Update Leave*/
//    Route::get('/leave/my-leave/{id}/edit-leave', 'leave\LeaveController@editLeave');
//    Route::get('/leave/my-leave/{id}/update-leave', 'leave\LeaveController@updateLeave');


});