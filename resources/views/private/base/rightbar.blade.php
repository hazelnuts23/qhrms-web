<div id="page-rightbar">

    <div id="chatarea">
        <div class="chatuser">
            <span class="pull-right">Jane Smith</span>
            <a id="hidechatbtn" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
        <div class="chathistory">
            <div class="chatmsg">
                <p>Hey! How's it going?</p>
                <span class="timestamp">1:20:42 PM</span>
            </div>
            <div class="chatmsg sent">
                <p>Not bad... i guess. What about you? Haven't gotten any updates from you in a long time.</p>
                <span class="timestamp">1:20:46 PM</span>
            </div>
            <div class="chatmsg">
                <p>Yeah! I've been a bit busy lately. I'll get back to you soon enough.</p>
                <span class="timestamp">1:20:54 PM</span>
            </div>
            <div class="chatmsg sent">
                <p>Alright, take care then.</p>
                <span class="timestamp">1:21:01 PM</span>
            </div>
        </div>
        <div class="chatinput">
            <textarea name="" rows="2"></textarea>
        </div>
    </div>

    <div id="widgetarea">
        <div class="widget">
            <div class="widget-heading">
                <a href="javascript:;" data-toggle="collapse" data-target="#accsummary"><h4>Account Summary</h4></a>
            </div>
            <div class="widget-body collapse in" id="accsummary">
                <div class="widget-block" style="background: #7ccc2e; margin-top:10px;">
                    <div class="pull-left">
                        <small>Position</small>
                        <h5>Human Resource Manager</h5>
                    </div>
                    <div class="pull-right">
                        <div id="currentbalance"></div>
                    </div>
                </div>
                <div class="widget-block" style="background: #595f69;">
                    <div class="pull-left">
                        <small>User Type</small>
                        <h5>Human Resource</h5>
                    </div>
                </div>
            </div>
        </div>


        <div class="widget">
            <div class="widget-heading">
                <a href="javascript:;" data-toggle="collapse" data-target="#taskbody">
                    <h4>Leave Balance</h4>
                </a>
            </div>
            <div class="widget-body collapse in" id="taskbody">
                <div class="contextual-progress" style="margin-top:10px;">
                    <div class="clearfix">
                        <div class="progress-title">Annual Leave</div>
                        <div class="progress-percentage">8 out of 16</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" style="width: 25%"></div>
                    </div>
                </div>
                <div class="contextual-progress">
                    <div class="clearfix">
                        <div class="progress-title">Medical Leave</div>
                        <div class="progress-percentage">16 days</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-primary" style="width: 17%"></div>
                    </div>
                </div>
                <div class="contextual-progress">
                    <div class="clearfix">
                        <div class="progress-title">Maternity Leave</div>
                        <div class="progress-percentage">16 days</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" style="width: 70%"></div>
                    </div>
                </div>
                <div class="contextual-progress">
                    <div class="clearfix">
                        <div class="progress-title">Paternity Leave</div>
                        <div class="progress-percentage">16 days</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger" style="width: 6%"></div>
                    </div>
                </div>
                <div class="contextual-progress">
                    <div class="clearfix">
                        <div class="progress-title">Compassionate Leave</div>
                        <div class="progress-percentage">16 days</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-orange" style="width: 20%"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>