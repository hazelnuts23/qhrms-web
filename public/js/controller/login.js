(function (window, document, $, angular) {
    var app = angular.modeul('qhrms', ['qhrms.app'], function ($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

    app.controller('login', function (loginService, $scope) {

        $scope.loginBtn = function () {
            var csrf_token = $scope.csrf_token;
            var login_email = $scope.login_email;
            var login_password = $scope.login_password;
            loginService.login(csrf_token, login_email, login_password).then(function (response) {

            })
        }

    })
});