<link rel="stylesheet" href="{{ URL::asset('/css/styles.css')}}">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
<script type='text/javascript' src='{{ URL::asset('js/jquery.min.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/jqueryui-1.10.3.min.js') }}'></script>
<script type="text/javascript" src='{{ URL::asset('js/html5.js') }}'></script>
<script type="text/javascript" src='{{ URL::asset('js/respond.min.js') }}'></script>
<script type="text/javascript" src='{{ URL::asset('js/plugins/charts-flot/excanvas.min.js') }}'></script>

<link rel='stylesheet' type='text/css' href='assets/plugins/form-select2/select2.css' />
<link rel='stylesheet' type='text/css' href='assets/plugins/form-multiselect/css/multi-select.css' />
<link rel='stylesheet' type='text/css' href='assets/plugins/jqueryui-timepicker/jquery.ui.timepicker.css' />
<link rel='stylesheet' type='text/css' href='assets/plugins/form-daterangepicker/daterangepicker-bs3.css' />
<link rel='stylesheet' type='text/css' href='assets/plugins/form-fseditor/fseditor.css' />
<link rel='stylesheet' type='text/css' href='assets/plugins/form-tokenfield/bootstrap-tokenfield.css' />
<link rel='stylesheet' type='text/css' href='assets/js/jqueryui.css' />
<link rel='stylesheet' type='text/css' href='assets/plugins/codeprettifier/prettify.css' />
<link rel='stylesheet' type='text/css' href='assets/plugins/form-toggle/toggles.css' />
<link rel='stylesheet' type='text/css' href='{{ URL::asset('plugins/codeprettifier/prettify.css')}}'/>
<link rel='stylesheet' type='text/css' href='{{ URL::asset('plugins/form-toggle/toggles.css')}}'/>


