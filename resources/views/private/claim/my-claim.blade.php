<?php use App\LeaveType;
use App\Approval;?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-sky">
            <div class="panel-heading">
                <div class="options">
                    <a href="javascript:;"><i class="fa fa-cog"></i></a>
                    <a href="javascript:;"><i class="fa fa-wrench"></i></a>
                    <a href="javascript:;" class="panel-collapse"><i class="fa fa-chevron-down"></i></a>
                </div>
            </div>
            <div class="panel-body collapse in">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                    <thead>
                    <tr>
                        <th>Applicant Name</th>
                        <th class="text-center" width="8%">Apply Date</th>
                        <th class="text-center" width="12%">Leave Type</th>
                        <th class="text-center" width="8%">From Date</th>
                        <th class="text-center" width="8%">To Date</th>
                        <th class="text-center" width="10%">Status</th>
                        <th class="text-center" width="12%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($myleave as $val)
                        <tr>
                            <td>{{ $val->name }}</td>
                            <td class="text-center">{{ date("d/m/Y", strtotime($val->date_applied)) }}</td>
                            <?php $type = LeaveType::find($val->leave_type); ?>
                            <td class="text-center">{{ $type->name  }}</td>
                            <td class="text-center">{{ date("d/m/Y", strtotime($val->start_date)) }}</td>
                            <td class="text-center">{{ date("d/m/Y", strtotime($val->end_date)) }}</td>
                            <?php $stat = Approval::find($val->leave_status) ?>
                            <td class="text-center">{{ $stat->description }}</td>
                            <td class="text-center">
                                <button id="updateLeave" onClick="updateLeave('{{ $val->leave_id }}')" class="btn btn-xs btn-success">
                                    Update
                                </button>
                                <button onclick="removeLeave('{{ $val->leave_id }}')" class="btn btn-xs btn-danger">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function updateLeave(val){
        window.location.href="{{ URL::to('leave/my-leave') }}/"+val+"/edit-leave";
    }
    function removeLeave(val){
        var r = confirm("Do you want to remove this Leave?");
        if(r==true){
            window.location.href="{{ URL::to('leave/my-leave') }}/"+val+"/delete"
        }
    }
</script>