<?php

Auth::routes();


Route::group(array('middleware' => 'auth'), function () {
    Route::get('/', 'dashboard\DashboardController@index');
    Route::get('/dashboard', 'dashboard\DashboardController@index');

    /* Leave */
    /*My Leave*/
    Route::get('/leave/my-leave', 'leave\LeaveController@myleave');
    Route::post('/leave/my-leave/save-leave', 'leave\LeaveController@savemyleave');
    Route::post('/leave/my-leave/{id}/{leave_type}/check_balance', 'leave\LeaveController@leavebalance');
    Route::get('/leave/my-leave/{id}/delete', 'leave\LeaveController@destroy');

    /*Apply Leave*/
    Route::get('/leave/apply-leave', 'leave\LeaveController@applyleave');
    Route::post('/leave/save-leave', 'leave\LeaveController@savemyleave');

    /*Update Leave*/
    Route::get('/leave/my-leave/{id}/edit-leave', 'leave\LeaveController@editLeave');
    Route::get('/leave/my-leave/{id}/update-leave', 'leave\LeaveController@updateLeave');



    /* Claim */
    Route::get('/applications', 'LeaveListControllerr@index');
    Route::get('/applications/{id}/approve', 'LeaveListController@approve');
    Route::get('/applications/{id}/reject', 'LeaveListController@reject');
    Route::get('/applications/{id}/delete', 'MyLeaveController@destroy');
    Route::get('/applications/{id}/{leave_type}/leavebalance', 'MyLeaveController@leavebalance');



    /*My-Leave*/
    Route::get('/my-claim','claim\ClaimController@myclaim' );

});

Route::get('/home', 'HomeController@index');
