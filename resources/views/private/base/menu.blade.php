<?php
$firstpath = Request::segment(1);

?>

<nav class="navbar navbar-default yamm" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse large-icons-nav" id="horizontal-navbar">
        <ul class="nav navbar-nav">
            <li @if($firstpath=='dashboard') {{ 'class=active' }} @endif><a href="{{ URL::to('dashboard') }}"><i class="fa fa-dashboard"></i>
                    <span>Dashboard</span></a></li>
            <li class="dropdown @if($firstpath=='leave') {{ 'active' }} @endif">
                <a href="#" class="dropdown-toggle" data-toggle='dropdown'><i class="fa fa-columns"></i> <span>Leave <i
                                class="fa fa-angle-down"></i></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ URL::to('leave/apply-leave') }}">Apply Leave</a></li>
                    <li><a href="{{ URL::to('leave/my-leave') }}">My Leave</a></li>
                </ul>
            </li>
<!--            <li class="dropdown @if($firstpath=='claim') {{ 'active' }} @endif">-->
<!--                <a href="#" class="dropdown-toggle" data-toggle='dropdown'><i class="fa fa-globe"></i> <span>Claim <i-->
<!--                                class="fa fa-angle-down"></i></span></a>-->
<!--                <ul class="dropdown-menu">-->
<!--                    <li><a href="{{ URL::to('claim/apply-claim') }}">Apply Claim</a></li>-->
<!--                    <li><a href="{{ URL::to('claim/my-claim') }}">My Claim</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="dropdown @if($firstpath=='training') {{ 'active' }} @endif">-->
<!--                <a href="#" class="dropdown-toggle" data-toggle='dropdown'><i class="fa fa-group"></i> <span>Training <i-->
<!--                                class="fa fa-angle-down"></i></span></a>-->
<!--                <ul class="dropdown-menu">-->
<!--                    <li><a href="{{ URL::to('training/my-training') }}">My Training</a></li>-->
<!--                </ul>-->
<!--            </li>-->
            <li  class="dropdown @if($firstpath=='configuration') {{ 'active' }} @endif">
                <a href="#" class="dropdown-toggle" data-toggle='dropdown'><i class="fa fa-cog"></i> <span>Configuration <i
                                class="fa fa-angle-down"></i></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Database Backup</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>