<div id="page-heading">

    <ol class="breadcrumb">
        @foreach($breadcrumb as $bc)
            <li>@if(!is_null($bc['url'])) <a href="{{ URL::to($bc['url']) }}">{{ $bc['name'] }}</a> @else {{ $bc['name'] }} @endif</li>
        @endforeach
    </ol>

    <h1>{{ $headingtitle }}</h1>

</div>