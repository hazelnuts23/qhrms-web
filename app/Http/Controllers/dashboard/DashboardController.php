<?php

namespace App\Http\Controllers\dashboard;

use Auth;
use DB;
use App\Http\Controllers\Controller;
use View;
use App\Model\Content;

class DashboardController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $user = DB::connection('mysql')->select('SELECT u.username, u.email, s.name FROM users u LEFT JOIN staff s ON u.staff_id=s.id WHERE u.id=?',
            [$user_id]);

        $breadcrumb = array(
            0 => array('name' => 'Dashboard', 'url' => 'dashboard'),
        );
        $heading_title = 'Dashboard';
        $content = Content::all();

        return View::make('private.index', array('breadcrumb' => $breadcrumb, 'headingtitle' => $heading_title))
            ->nest('content', 'private.dashboard.dashboard', array('content' => $content, 'user' => $user))
            ->nest('header_script', 'private.base.headerscript')
            ->nest('footer_script', 'private.base.footerscript_dashboard');

    }
}
