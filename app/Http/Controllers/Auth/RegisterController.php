<?php

namespace App\Http\Controllers\Auth;

use App\Model\Staff;
use App\Model\User;
use App\Model\LeaveBalance;
use App\Model\LeaveType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $leaveTypeArray = LeaveType::all();
        $staff = Staff::create(
            [
                'name' => $data['name'],
            ]);
        $staff_id = $staff->id;
        foreach ($leaveTypeArray as $leaveType) {
            LeaveBalance::create(
                [
                    'staff_id' => $staff_id,
                    'balance' => $leaveType['default_balance'],
                    'leave_type' => $leaveType['id'],
                    'year' => date('Y'),
                    'flag' => 0,
                ]
            );
        }

        return User::create([
            'username' => $data['email'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'staff_id' => $staff_id,
            'level' => 1,
            'status' => 1,

        ]);
    }
}
