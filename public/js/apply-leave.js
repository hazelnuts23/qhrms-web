jQuery(function ($) {
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#startDate').datepicker({
        format: "dd/mm/yyyy",
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#endDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#endDate').datepicker({
        format: "dd/mm/yyyy",
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');

    $('input:radio[name="leavetype"]').change(
        function () {
            if ($(this).val() != '') {
                var id = $(this).val();
                $.ajax
                ({
                    headers:
                    {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    type: "POST",
                    url:  $('input[name="_checkbalance"]').val()+'/'+id+'/check_balance',
                    // dataType: "json",
                    beforeSend: function () {
                        $.blockUI();
                        $('.inner-circles-loader').show();

                    },
                    success: function (data) {
                        if (data.status == 1) {
                            $("#balance_text").html(data.value);
                            $('.inner-circles-loader').hide();
                            $.unblockUI();
                        } else {
                            $("#balance_text").html("0");
                            $('.inner-circles-loader').hide();
                            $.unblockUI();
                        }

                    }
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    alert('No response from server');
                    $.unblockUI();
                });
            }
        }
    );

    $('#apply-leave').submit(function () {
        $.ajax({
                type: "post",
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                data: $('#apply-leave').serialize(),
                datatype: "json",
                beforeSend: function () {
                    $.blockUI();
                }
            })
            .done(function (data) {
                if (data.status == 0) {
                    var arr = data.errors;
                    $.each(arr, function (index, value) {
                        if (value.length != 0) {
                            $("#" + index).addClass('has-error');
                            $("#" + index + " .help-block").html(value);
                        }
                    });
                    $.unblockUI();
                }
                else {
                    // close modal

                    window.location.href = data.redirect;
                    $.unblockUI();
                }
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
                $.unblockUI();
            });
        return false;
    });

});