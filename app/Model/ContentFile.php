<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContentFile extends Model
{
    protected $table = 'content_file';
    protected $primaryKey = 'id';
}
