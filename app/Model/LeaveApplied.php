<?php

namespace App\Model;

use Auth;
use Illuminate\Database\Eloquent\Model;
use App\Model\LeaveType;
use App\Model\LeaveBalance;
use DB;

class LeaveApplied extends Model
{
    protected $table = 'leave_applied';
    protected $primaryKey = 'id';
    protected $fillable = ['start_date', 'end_date', 'applied_by', 'leave_type', 'reason', 'leave_day', 'flag', 'status','date_applied'];

    public static function countLeaveApplied()
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;

            return LeaveApplied::select('*')
                ->where('applied_by', $user_id)
                ->count();
        } else {
            return null;
        }

    }

    public static function countLeaveAppliedApi($userId)
    {
        return LeaveApplied::select('*')
            ->where('applied_by', $userId)
            ->count();

    }

    public static function getAppliedPendingListApi($organizationId)
    {
        return LeaveApplied::Select('leave_applied.id as leave_id',
            \DB::raw('DATE_FORMAT(leave_applied.start_date, \'%d-%m-%Y\') as start_date'),
            \DB::raw('DATE_FORMAT(leave_applied.end_date, \'%d-%m-%Y\') as end_date'),
            \DB::raw('DATE_FORMAT(leave_applied.date_applied, \'%d-%m-%Y\') as date_applied'),
            'leave_applied.leave_day',
            'approval.description as leave_status',
            'leave_type.name as leave_name',
            'staff.name As staff_name',
            'staff.id as staff_id')
            ->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
            ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
            ->leftJoin('staff_profile as sp', 'sp.staff_id', '=', 'staff.id')
            ->leftJoin('approval', 'approval.id', '=', 'leave_applied.status')
            ->leftJoin('leave_type', 'leave_type.id', '=', 'leave_applied.leave_type')
            ->where('leave_applied.status', 3)
            ->where('staff.organization_id', $organizationId)
            ->get();
    }


    public static function getAppliedListCurrentMonthApi($userId)
    {
        $currentMonth = date('n');
        $leaveApplied = LeaveApplied::select('leave_applied.id as leave_id',
            \DB::raw('DATE_FORMAT(leave_applied.start_date, \'%d-%m-%Y\') as start_date'),
            \DB::raw('DATE_FORMAT(leave_applied.end_date, \'%d-%m-%Y\') as end_date'),
            \DB::raw('DATE_FORMAT(leave_applied.date_applied, \'%d-%m-%Y\') as date_applied'),
            'leave_applied.leave_day',
            'approval.description as leave_status',
            'leave_type.name as leave_name',
            'staff.name As staff_name',
            'staff.id as staff_id')
            ->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
            ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
            ->leftJoin('staff_profile as sp', 'sp.staff_id', '=', 'staff.id')
            ->leftJoin('approval', 'approval.id', '=', 'leave_applied.status')
            ->leftJoin('leave_type', 'leave_type.id', '=', 'leave_applied.leave_type')
            ->where('users.id', $userId)
            ->whereMonth('leave_applied.date_applied', $currentMonth)
            ->get();

        if ($leaveApplied) {
            return $leaveApplied;
        } else {
            return null;
        }
    }

    public static function getAllMyLeave($userId)
    {
        $leaveApplied = LeaveApplied::select('leave_applied.id as leave_id',
            \DB::raw('DATE_FORMAT(leave_applied.start_date, \'%d-%m-%Y\') as start_date'),
            \DB::raw('DATE_FORMAT(leave_applied.end_date, \'%d-%m-%Y\') as end_date'),
            \DB::raw('DATE_FORMAT(leave_applied.date_applied, \'%d-%m-%Y\') as date_applied'),
            'leave_applied.leave_day',
            'approval.description as leave_status',
            'leave_type.name as leave_name',
            'staff.name As staff_name',
            'staff.id as staff_id')
            ->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
            ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
            ->leftJoin('staff_profile as sp', 'sp.staff_id', '=', 'staff.id')
            ->leftJoin('approval', 'approval.id', '=', 'leave_applied.status')
            ->leftJoin('leave_type', 'leave_type.id', '=', 'leave_applied.leave_type')
            ->where('users.id', $userId)
            ->get();

        if ($leaveApplied) {
            return $leaveApplied;
        } else {
            return null;
        }
    }

    public static function getMyAppliedListApi($userId)
    {
        return LeaveApplied::Select('*', 'leave_applied.status As leave_status',
            'leave_applied.id As leave_id')->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
            ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
            ->leftJoin('staff_profile as sp', 'sp.staff_id', '=', 'staff.id')
            ->where('leave_applied.applied_by', $userId)
            ->get();
    }

    public static function getMyAppliedList()
    {
        $list = false;
        if (Auth::check()) {
            $list = LeaveApplied::Select('*', 'leave_applied.status As leave_status',
                'leave_applied.id As leave_id')->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
                ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
                ->leftJoin('staff_profile as sp', 'sp.staff_id', '=', 'staff.id')
                ->where('leave_applied.applied_by', Auth::user()->id)
                ->get();
        }

        return $list;
    }

    public static function getAppliedList()
    {
        $list = LeaveApplied::Select('*', 'leave_applied.status As leave_status',
            'leave_applied.id As leave_id')->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
            ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
            ->leftJoin('staff_profile as sp', 'sp.staff_id', '=', 'staff.id')
            ->get();

        return $list;
    }

    public static function getAppliedListCurrentMonth()
    {


        $currentMonth = date('n');
        if (Auth::check()) {
            $staff_id = Auth::user()->staff->id;
            $leaveApplied = LeaveApplied::Select('*', 'leave_applied.status As leave_status',
                'leave_applied.id As leave_id')->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
                ->leftJoin('staff', 'staff.id', '=', 'users.staff_id')
                ->leftJoin('staff_profile as sp', 'sp.staff_id', '=', 'staff.id')
                ->where('users.staff_id', $staff_id)
                ->whereMonth('leave_applied.date_applied', $currentMonth)
                ->get();

            if ($leaveApplied) {
                return $leaveApplied;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static function getAppliedByUserID($val)
    {
        return LeaveApplied::Select('*', 'leave_applied.status As leave_status',
            'leave_applied.id As leave_id')->leftJoin('users', 'users.id', '=', 'leave_applied.applied_by')
            ->leftJoin('user_profile', 'user_profile.id', '=', 'users.profile_id')
            ->where('leave_applied.applied_by', '=', $val)
            ->get();
    }

    public static function getLeaveBalance($applied_by, $type)
    {

        $type = LeaveBalance::where('id', '=', $type)->where('staff_id', '=', $applied_by)->first();
        if ($type) {
            $balance = LeaveApplied::select(DB::raw('sum(leave_day) as leave_sum'))->where('leave_type',
                $type->id)->where('applied_by', $applied_by)->where('status', 1)->first();
            $bl = $type->balance;
            $bd = $balance->leave_sum;
            $final = $bl - $bd;
        } else {
            $final = $type['balance'];
        }


        return $final;
    }
}
