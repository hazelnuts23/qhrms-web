<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Model\User;
use App\Model\Staff;
use Illuminate\Http\Request;
use App\Model\LeaveApplied;
use App\Model\LeaveBalance;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    protected function index(Request $request)
    {
        $input = $request->input();
        $userId = $input['userid'];
        $user = User::select('staff_id', 'level')->find($userId);
        $staff = Staff::find($user['staff_id']);

        $countLeaveApplied = LeaveApplied::countLeaveAppliedApi($userId);
        $myAppliedCurrentMonth = LeaveApplied::getAppliedListCurrentMonthApi($userId);
        $leaveListPending = null;
        if ($user['level'] == 1 || $user['level'] == 3 || $user['level'] == 4) {
            $leaveListPending = LeaveApplied::getAppliedPendingListApi($staff['organization_id']);

            if (count($leaveListPending) == 0) {
                $leaveListPending = 0;
            }
        }
        $leaveBalance = LeaveBalance::allLeaveBalanceApi($userId);
        $leaveArray = array(
            'user_level' => $user['level'],
            'leave_balance' => $leaveBalance,
            'current_month_leave_count' => $countLeaveApplied,
            'leave_list_pending' => $leaveListPending,
            'applied_current_month' => $myAppliedCurrentMonth,
        );

        return response()->json($leaveArray);
    }
}
