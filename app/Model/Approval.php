<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    protected $table = 'approval';
    protected $primaryKey = 'id';

    public static function getDescription($val){
        return Approval::where('id', $val)->first();
    }
}
