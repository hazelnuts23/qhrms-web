<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-midnightblue">
            <form action="{{ URL::to('leave/save-leave') }}" method="POST" class="form-horizontal" id="apply-leave" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">{{ 'Leave Type' }}</label>

                        <div class="col-sm-6">
                            @foreach($leavetype as $ltval)
                                <label class="radio-inline">
                                    <input type="radio" name="leavetype" id="leavetype"
                                           value="{{ $ltval->id }}"> {{ $ltval->name }}
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{{ 'Available Balance' }}</label>

                        <div class="col-sm-6">
                            <span id="balance_text">0</span> day(s)
                            <input type="hidden" name="_checkbalance"
                                   value="{{ URL::to('leave/my-leave/'.Auth::user()->id) }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{{ 'Reason for Leave' }}</label>

                        <div class="col-sm-6">
                            <input type="text" name="reason" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{{ 'Date Leave' }}</label>

                        <div class="col-sm-1">
                            <input type="text" name="start_date" id="startDate"
                                   class="form-control"></div>
                        <div class="pull-left">{{ 'to ' }}</div>
                        <div class="col-sm-1"><input type="text"
                                                     id="endDate"
                                                     name="end_date"
                                                     class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{{ 'Remarks' }}</label>

                        <div class="col-sm-6">
                            <textarea name="remarks" class="form-control"></textarea>
                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="btn-toolbar">
                                <button class="btn-primary btn" type="submit">Send
                                </button>
                                <button class="btn-default btn">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type='text/javascript' src='{{ URL::to('js/update-leave.js') }}'></script>