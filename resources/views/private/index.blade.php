<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Human Resource Management System</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Avant">
    <meta name="author" content="The Red Team">


    <?php echo $header_script;?>

</head>

<body class="horizontal-nav ">

<div id="headerbar">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-6">
                <a href="#" class="shortcut-tiles tiles-brown">
                    <div class="tiles-body">
                        <div class="pull-left"><i class="fa fa-pencil"></i></div>
                    </div>
                    <div class="tiles-footer">
                        Create Post
                    </div>
                </a>
            </div>
            <div class="col-xs-6 col-sm-6">
                <a href="#" class="shortcut-tiles tiles-grape">
                    <div class="tiles-body">
                        <div class="pull-left"><i class="fa fa-group"></i></div>
                        <div class="pull-right"><span class="badge">2</span></div>
                    </div>
                    <div class="tiles-footer">
                        Staff
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>


@include('private.base.header')

@include('private.base.menu')


<div id="page-container">
    @include('private.base.rightbar')


    <div id="page-content">
        <div id="wrap">
            @include('private.base.heading')
            <div class="container">
                <?php echo $content;?>
            </div>
        </div>
    </div>
    @include('private.base.footer')

</div>

<?php echo $footer_script;?>
</body>
</html>