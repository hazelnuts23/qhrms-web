jQuery(document).ready(function () {
    $('.inner-circles-loader').hide();
    $('#login-action').submit(function () {
        $.ajax({
            url: $('#login-action').attr("action"),
            type: "post",
            headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
            data: $('#login-action').serialize(),
            datatype: "json",
            beforeSend: function () {
                $.blockUI();
                $('.inner-circles-loader').show();
            }
        })
            .done(function (data) {
                if (data.status == 0) {
                    var arr = data.errors;
                    $.each(arr, function (index, value) {
                        if (value.length != 0) {
                            $("#" + index).addClass('has-error');
                            $("#" + index + " .help-block").html(value);
                        }
                    });
                    $('.inner-circles-loader').hide();
                    $.unblockUI();
                }
                else {
                    // close modal
                    window.location.href = data.redirect;
                    $('.inner-circles-loader').hide();
                    $.unblockUI();
                }
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
                $.unblockUI();
            });
        return false;
    });
});